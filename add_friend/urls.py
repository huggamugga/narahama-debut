from django.conf.urls import url
from .views import index, add_new_friend

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-new-friend', add_new_friend, name='add-new-friend'),
]