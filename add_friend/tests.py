from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Friend
from .views import index, add_new_friend
from .forms import Add_Friend_Form

# Create your tests here.
class AddFriendUnitTest(TestCase):

    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_friend(self):
        # Creating a new activity
        new_activity = Friend.objects.create(name='kevin',url="https://kapeganteng.herokuapp.com/")

        # Retrieving all available activity
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_add_friend_post_success_and_render_the_result(self):
        test_name = 'kevin'
        test_link = 'https://kapeganteng.herokuapp.com/'
        response_post = Client().post('/add-friend/add-new-friend', {'name': test_name, 'url': test_link})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(test_name, html_response)
        self.assertIn(test_link, html_response)

    def test_add_friend_post_error_and_render_the_result(self):
        test = 'test case error'
        response_post = Client().post('/add-friend/add-new-friend', {'name': '','url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)