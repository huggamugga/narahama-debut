from django.shortcuts import render, redirect
from .forms import Add_Friend_Form
from .models import Friend
from django.http import HttpResponseRedirect

response = {}

def index(request):
    html = 'add_friend.html'
    response['message_form'] = Add_Friend_Form
    response['friend'] = Friend.objects.all()
    return render(request, html, response)

def add_new_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=response['name'],url=response['url'])
        friend.save()
        return HttpResponseRedirect('/add-friend/')
    else:
        return HttpResponseRedirect('/add-friend/')