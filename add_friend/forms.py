from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan URL',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='url', required=True,widget=forms.TextInput(attrs=attrs))