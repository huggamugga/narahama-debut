Kelompok 11 : Narahama

Anggota :
1. Aldo Bima Syahputra 	(1606833463)
2. Hanif Agung Prayoga 	(1606884344)
3. Nadia Nabila Anjani 	(1606874463)
4. Sarah Yunita 	(1606889282)

MARKDOWN PIPELINE : [![pipeline status](https://gitlab.com/huggamugga/narahama-debut/badges/master/pipeline.svg)](https://gitlab.com/huggamugga/narahama-debut/commits/master)
MARKDOWN COVERAGE : [![coverage report](https://gitlab.com/huggamugga/narahama-debut/badges/master/coverage.svg)](https://gitlab.com/huggamugga/narahama-debut/commits/master)

Link Heroku : narahamaa.herokuapp.com