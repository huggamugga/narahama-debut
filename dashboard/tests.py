from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .views import index

class DashboardUnitTest(TestCase):
	def test_dashboard_is_exist(self):
		response = Client().get('/statistic/')
		self.assertEqual(response.status_code, 200)
		
	def test_dashboard_using_index_funct(self):
		found = resolve('/statistic/')
		self.assertEqual(found.func, index)