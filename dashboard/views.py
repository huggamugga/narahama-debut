from django.shortcuts import render

from update_status.models import Todo #nyari status pertama dan jumlah feeds
from halaman_profile.models import halamanProfile #nyari foto
from add_friend.models import Friend #nyari jumlah teman
from halaman_profile.views import profile

from add_friend.forms import Add_Friend_Form #nyari temen

response = {}
def index(request):
	response['jumlahteman'] = Friend.objects.all().count()
	response['profile'] = profile
	response['statusterakhir'] = Todo.objects.all().last()
	response['jumlahstatus'] = Todo.objects.all().count()
	return render(request, 'dashboard.html', response)