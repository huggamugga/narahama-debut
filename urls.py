from django.conf.urls import url
from .views import index, add_todo

import dashboard.urls as dashboard


urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^add_todo', add_todo, name='add_todo'),
	url(r'^statistic/', include(dashboard, namespace = 'dashboard')),
]
