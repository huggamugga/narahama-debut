from django.shortcuts import render
from .models import halamanProfile

profile = halamanProfile(name = "Narahama Lalala", birthday = "1998-10-04", gender = "Female", expertise = "Coding, Writing, Singing", description = "A college student in her second year", email = "narahama@ui.ac.id")
response = {"name": profile.name, "birthday": profile.birthday, "gender": profile.gender, "expertise": profile.expertise, "description": profile.description, "email": profile.email}

def index(request):
	return render(request, 'halaman_profile.html', response)


# Create your views here.
