from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from .views import profile, response, index
from unittest import skip

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_halaman_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_bio_is_fulfilled(self):
        self.assertIsNotNone(response)
        self.assertTrue(len(response) >= 3)
        for key, value in response.items():
            self.assertIsNotNone(key)
            self.assertIsNotNone(value)
            self.assertTrue(len(value) > 3)

   #     def test_root_url_now_is_using_index_page_from_lab_2(self):
   #     response = Client().get('/')
   #     self.assertEqual(response.status_code, 301)
   #     self.assertRedirects(response,'/profile/',301,200)