from django.db import models

class halamanProfile(models.Model):
	name = models.CharField(max_length = 150)
	birthday = models.DateField(default = '1998-10-04')
	gender = models.CharField(max_length = 6)
	expertise = models.CharField(max_length = 250)
	description = models.CharField(max_length = 250)
	email = models.CharField(max_length = 150)